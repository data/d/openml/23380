# OpenML dataset: cjs

https://www.openml.org/d/23380

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Dr. Fernando Camacho  
**Source**: Unknown - 1995  
**Please cite**: Camacho, F. and Arron, G. (1995)  Effects of the regulators paclobutrazol and flurprimidol on the growth of terminal sprouts formed on trimmed silver maple trees. Canadian Journal of Statistics 3(23).

Data on tree growth used in the Case Study published in the September, 1995 issue of the Canadian Journal of Statistics. This data set was been provided by Dr. Fernando Camacho, Ontario Hydro Technologies, 800 Kipling Ave, Toronto Canada M3Z 5S4. It forms the basis of the Case Study in Data Analysis published in the Canadian Journal of Statistics, September 1995. It can be freely used for noncommercial purposes, as long as proper acknowledgement to the source and to the Canadian Journal of Statistics is made.


Description


The effects of the Growth Regulators Paclobutrazol (PP 333)
and Flurprimidol (EL-500) on the Number and Length of Internodes
in Terminal Sprouts Formed on Trimmed Silver Maple Trees.
 
Introduction:
 
The trimming of trees under distribution lines on city streets and
in rural areas is a major problem and expense for electrical
utilities.  Such operations are routinely performed at intervals of
one to eight years depending upon the individual species growth rate
and the amount of clearance required.  Ontario Hydro trims about
500,000 trees per year at a cost of about $25 per tree.
 
Much effort has been spent in developing chemicals for the horticultural
industry to retard the growth of woody and herbaceous plants.  Recently,
a group of new growth regulators was introduced which was shown to be
effective in controlling the growth of trees without producing
noticeable injury symptoms.  In this group are PP 333 ( common name
paclobutrazol) (2RS, 3RS - 1 -(4-chlorophenyl) - 4,4 - dimethyl - 2 -
(1,2,4-triazol-l-yl) pentan - 3- ol and EL-500 (common name flurprimidol
and composition alpha - (1-methylethyl) - alpha - [4-(trifluromethoxyl)
phenyl] - 5- pyrimidine - methanol).  Both EL-500 and PP-333 have been
reported to control excessive sprout growth in a number of species
when applied as a foliar spray, as a soil drench, or by trunk injection.
Sprout length is a function of both the number of internodes and
the length of the individual internodes in the sprout.  While there
have been many reports that both PP 333 and EL-500 cause a reduction
in the length of internodes formed in sprouts on woody plants treated
with the growth regulators, there has been but one report that EL-500
application to apple trees resulted in a reduction of the number
of internodes formed per sprout.
 
The purpose of the present study was to investigate the length of the
terminal sprouts, the length of the individual internodes in those
sprouts, and the number of internodes in trimmed silver maple trees
following trunk injection with the growth regulators PP 333 and EL-500.
 
Experimental Details.
 
Multistemmed 12-year-old silver maple trees growing at Wesleyville,
Ontario were trunk injected with methanolic solutions of EL-500
and PP-333 in May of 1985 using a third generation Asplundh
tree injector.
 
Two different application rates (20 g/L and 4 g/L) were used for each
chemical.  The volume of solution (and hence the amount of active
ingredient) injected into each tree was determined from the diameter
of the tree, using the formula: vol(mL) = (dbh)*(dbh)*.492 where dbh
is the diameter at breast height.  Two sets of control trees were
included in the experiment.  In one set, tree received no injection
(control) and in a second set, the trees were injected with
methanol, the carrier in the growth regulator solutions.  Ten trees,
chosen at random, were used in each of the control and experimental
sets.  Prior to injection, all the trees were trimmed by a forestry
crew, with their heights being reduced by about one third.
 
In January 1987, twenty months after the trees were injected, between
six and eight limbs were removed at random from the bottom two-thirds
of the canopy of each of the ten trees in each experimental and control
set.  The limbs were returned to the laboratory and the length of all
the terminal sprouts, the lengths of the individual internodes, and
the number of internodes recorded.  Between one and 25 terminal
sprouts were found on each limb collected.  Sprouts which had a
length of 1 cm or less were recorded as being 1 cm in length.
In such spouts, the internode lengths were not measured, but were
calculated from the total length of the sprout and the number
of internodes counted.  Internode lengths were then expressed to one
decimal place.  In two instances, one of the ten trees in a set
could not be sampled because limb removal would have jeopardized the
health of the tree over the long-term.
 
Data set:
 
Each of the records represents a terminal sprout and contains the
following information:
   N   the sprout number
   TR  treatment 1  control
                  2  methanol control
                  3  PP 333 20g/L
                  4  PP 333  4g/L
                  5  EL 500 20g/L
                  6  EL 500  4g/L
   TREE  tree id
   BR    branch id
   TL    total sprout length (cm)
   IN    number of internodes on the sprout
   INTER a list of the lengths of the internodes in the sprout,
          starting from the base of the sprout (129 entries)
 
Sprouts 1868 to 1879 do not have branch identification data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/23380) of an [OpenML dataset](https://www.openml.org/d/23380). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/23380/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/23380/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/23380/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

